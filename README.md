# Mimir thumbnailer (VIPS)

This app is a Mimir processor that generates cover images for indexed files.

For more information about how to use it see [documentation](https://gomimir.gitlab.io/processors/thumbnailer-vips).

This is optimized version of the processor using [libvips](https://github.com/libvips/libvips). It offers better performance but does not support all the file formats Imagick does. If you are generating thumbnails just for the JPEG and HEIC images then this is a good choice. If you are looking for generating thumbnails for RAW files or stuff like PDFs, choose [Imagick version](https://gitlab.com/gomimir/processor-imagick) instead.


## Developing in docker container

This repo is prepared to be developed [inside a container](https://code.visualstudio.com/docs/remote/containers).

Sample `.devcontainer/devcontainer.json` (see https://aka.ms/devcontainer.json)

```json
{
	"name": "Existing Dockerfile",

	"build": {
		"target": "dev"
	},

	"dockerFile": "../Dockerfile",

	"settings": { 
		"go.toolsManagement.checkForUpdates": "local",
		"go.useLanguageServer": true,
		"go.gopath": "/go",
		"go.goroot": "/usr/local/go"
	},
	
	"extensions": [
		"golang.Go"
	],

	// Note: replace with your host IP if you wish to test against local server
	"containerEnv": {
		"TZ": "Europe/Prague",
		"MIMIR_SERVER_HOST": "192.168.3.234:3001",
		"MIMIR_REDIS_HOST": "192.168.3.234:6379"
	},

	// For mounting processor lib
	// "mounts": [ "source=/Users/adam/Development/mimir/processor,target=/workspaces/processor,type=bind" ],
}
```

# Processing from docker container

Note: replace with your host IP

```bash
export MIMIR_SERVER_HOST=192.168.3.234:3001
export MIMIR_REDIS_HOST=192.168.3.234:6379
make run
```
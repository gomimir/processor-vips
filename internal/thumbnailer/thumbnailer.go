package thumbnailer

import (
	"fmt"
	"io"
	"time"

	"github.com/davidbyttow/govips/v2/vips"
	"github.com/rs/zerolog/log"
	mimir "gitlab.com/gomimir/processor"
)

type Request struct {
	Width  *int
	Height *int
	Format mimir.ThumbnailFileFormat
}

type Thumbnail struct {
	mimir.ThumbnailDescriptor
	Bytes []byte
}

func GetThumbnails(sourceReader io.ReadCloser, requests []Request) ([]*Thumbnail, error) {
	sourceBytes, err := io.ReadAll(sourceReader)
	if err != nil {
		log.Error().Err(err).Msg("Unable to read image content")
		return nil, err
	}

	sourceReader.Close()

	start := time.Now()

	sourceImage, err := vips.NewImageFromBuffer(sourceBytes)
	if err != nil {
		log.Error().Err(err).Msg("Unable to load image")
		return nil, err
	}

	orient := sourceImage.Orientation()
	err = sourceImage.AutoRotate()
	if err != nil {
		log.Error().Err(err).Msg("Unable to auto rotate the image")
		return nil, err
	}

	width := sourceImage.Width()
	height := sourceImage.Height()

	log.Debug().Int("width", width).Int("height", height).Msg("Image loaded")

	res := make([]*Thumbnail, len(requests))

	for i, req := range requests {
		image := sourceImage
		if i < len(requests)-1 {
			image, err = image.Copy()
			if err != nil {
				return nil, err
			}
		}

		var scaleRatio float64 = 1

		if req.Width != nil {
			scaleRatio = float64(*req.Width) / float64(width)
		}

		if req.Height != nil {
			hScaleRatio := float64(*req.Height) / float64(height)
			if hScaleRatio < scaleRatio {
				scaleRatio = hScaleRatio
			}
		}

		res[i] = &Thumbnail{
			ThumbnailDescriptor: mimir.ThumbnailDescriptor{
				Format: req.Format,
				Width:  int(width),
				Height: int(height),
			},
		}

		if scaleRatio < 1 {
			res[i].Width = int(float64(width) * scaleRatio)
			res[i].Height = int(float64(height) * scaleRatio)

			d := log.Debug().Int("width", res[i].Width).Int("height", res[i].Height).Float64("ratio", scaleRatio)
			if req.Width != nil && req.Height != nil {
				d.Msg("Scaling down to fit")
			} else if req.Width != nil {
				d.Msg("Scaling down to width")
			} else {
				d.Msg("Scaling down to height")
			}

			err = image.Thumbnail(res[i].Width, res[i].Height, vips.InterestingAll)
			if err != nil {
				log.Error().Err(err).Msg("Error occurred while trying to scale down the image")
				return nil, err
			}
		}

		if req.Format == mimir.TF_JPEG {
			if image.Format() == vips.ImageTypeJPEG && orient == 1 && scaleRatio >= 1 {
				// No change, no need to re-export
				res[i].Bytes = sourceBytes
			} else {
				res[i].Bytes, _, err = image.ExportJpeg(vips.NewJpegExportParams())
				if err != nil {
					log.Error().Err(err).Msg("Error occurred while exporting JPEG image")
					return nil, err
				}
			}
		} else if req.Format == mimir.TF_WebP {
			res[i].Bytes, _, err = image.ExportWebp(vips.NewWebpExportParams())
			if err != nil {
				log.Error().Err(err).Msg("Error occurred while exporting JPEG image")
				return nil, err
			}
		}
	}

	timeSpent := time.Since(start)
	log.Debug().Str("took", fmt.Sprintf("%v", timeSpent)).Msgf("Finished %v requests", len(requests))

	return res, nil
}

TAG_COMMIT := $(shell git rev-list --abbrev-commit --tags --max-count=1)
TAG := $(shell git describe --abbrev=0 --tags ${TAG_COMMIT} 2>/dev/null || true)
COMMIT := $(shell git rev-parse --short HEAD)
DATE := $(shell git log -1 --format=%cd --date=format:"%Y%m%d")
VERSION := $(TAG:v%=%)

ifeq ($(VERSION),)
    VERSION := 0.0.1-next-$(COMMIT)-$(DATE)
else
	ifneq ($(COMMIT), $(TAG_COMMIT))
			VERSION := $(VERSION)-next-$(COMMIT)-$(DATE)
	endif
endif
ifneq ($(shell git status --porcelain),)
    VERSION := $(VERSION)-dirty
endif

FLAGS := -ldflags "-X main.version=$(VERSION)"
INPUT := cmd/processor-vips/processor.go

version:
	@echo Version: $(VERSION)

build:
	go build $(FLAGS) -o ./bin/mimir-processor-vips $(INPUT)

run:
	go run $(FLAGS) $(INPUT) process

.PHONY: version build run
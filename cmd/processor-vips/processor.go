package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"time"

	"github.com/davidbyttow/govips/v2/vips"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	mimir "gitlab.com/gomimir/processor"
	thumbnailer "gitlab.com/gomimir/processor-vips/internal/thumbnailer"
	mimirapp "gitlab.com/gomimir/processor/pkg/app"
	mimirpb "gitlab.com/gomimir/processor/protobuf"
	mimirpbconv "gitlab.com/gomimir/processor/protobufconv"
)

// populated by build flags
var version string

type taskConfig struct {
	Thumbnails []thumbnailer.Request
}

type taskParams struct {
	Config taskConfig
}

func main() {
	app := mimirapp.NewProcessorApp("mimir-processor-vips", version)
	app.SetDefaultProcessingQueue("vips")

	rfp := app.RegisterFileProcessor("generateThumbnails", handler)

	testCmd := rfp.TestCommand()
	testCmd.Flags().Int("width", 0, "requested thumbnail width")
	testCmd.Flags().Int("height", 0, "requested thumbnail width")
	testCmd.Flags().String("format", string(mimir.TF_JPEG), "output file format")
	testCmd.Flags().StringP("out", "o", "", "output thumbnail into a file")

	testCmd.PreRun = func(cmd *cobra.Command, args []string) {
		initVips()
	}

	app.OnProcessorStart = func() {
		initVips()
	}

	app.Execute()
}

func initVips() {
	vips.LoggingSettings(func(messageDomain string, messageLevel vips.LogLevel, message string) {
		switch messageLevel {
		case vips.LogLevelError:
			log.Error().Msgf("%v: %v", messageDomain, message)
		case vips.LogLevelWarning:
			log.Warn().Msgf("%v: %v", messageDomain, message)
		case vips.LogLevelDebug:
			log.Debug().Msgf("%v: %v", messageDomain, message)
		case vips.LogLevelCritical:
			log.Fatal().Msgf("%v: %v", messageDomain, message)
		default:
			log.Info().Msgf("%v: %v", messageDomain, message)
		}
	}, vips.LogLevelMessage)

	vips.Startup(&vips.Config{})
}

func handler(req mimirapp.FileProcessorRequest) error {
	params := &taskParams{}

	if req.IsTest {
		width, _ := req.Command.Flags().GetInt("width")
		height, _ := req.Command.Flags().GetInt("height")
		format, _ := req.Command.Flags().GetString("format")

		var req thumbnailer.Request
		req.Format = mimir.ThumbnailFileFormat(format)

		if width != 0 {
			req.Width = &width
		}

		if height != 0 {
			req.Height = &height
		}

		if !req.Format.IsValid() {
			log.Error().Msgf("Invalid output file format: %v", format)
			return fmt.Errorf("invalid output file format: %v", format)
		}

		params.Config.Thumbnails = []thumbnailer.Request{
			req,
		}
	} else {
		err := req.Task.Parameters().LoadTo(params)
		if err != nil {
			req.Log.Error().Err(err).Msg("Unable to parse task params")
			return err
		}

		// Default format
		for i, thumbConfig := range params.Config.Thumbnails {
			if string(thumbConfig.Format) == "" {
				params.Config.Thumbnails[i].Format = mimir.TF_JPEG
			}
		}
	}

	r, err := req.Task.File().Reader()
	if err != nil {
		return err
	}

	thumbnails, err := thumbnailer.GetThumbnails(r, params.Config.Thumbnails)

	if err != nil {
		return err
	}

	if req.IsTest {
		if out, err := req.Command.Flags().GetString("out"); err == nil && out != "" {
			err = ioutil.WriteFile(out, thumbnails[0].Bytes, 0640)
			if err != nil {
				req.Log.Fatal().Err(err).Msg("Unable to write thumbnail output file")
				return err
			}
		}

		return nil
	} else {
		result := make([]*mimirpb.UploadAssetSidecarRequest, len(params.Config.Thumbnails))
		for i, thumb := range thumbnails {
			thumbReq := params.Config.Thumbnails[i]
			filename := fmt.Sprintf("fullres.%v", thumb.Format.FileSuffix())
			if thumbReq.Width != nil && thumbReq.Height != nil {
				filename = fmt.Sprintf("w%vh%v.%v", *thumbReq.Width, *thumbReq.Height, thumbReq.Format.FileSuffix())
			} else if thumbReq.Width != nil {
				filename = fmt.Sprintf("w%v.%v", *thumbReq.Width, thumbReq.Format.FileSuffix())
			} else if thumbReq.Height != nil {
				filename = fmt.Sprintf("h%v.%v", *thumbReq.Height, thumbReq.Format.FileSuffix())
			}

			thumbDesc, err := mimirpbconv.ThumbnailDescToProtoBuf(thumb.ThumbnailDescriptor)
			if err != nil {
				req.Log.Error().Err(err).Msg("Error occurred while preparing the GRPC request")
				return err
			}

			result[i] = &mimirpb.UploadAssetSidecarRequest{
				SourceFile: &mimirpb.FileRef{
					Repository: req.Task.File().FileRef().Repository,
					Filename:   req.Task.File().FileRef().Filename,
				},
				Filename: filename,
				Bytes:    thumb.Bytes,
				SidecarDescriptor: &mimirpb.UploadAssetSidecarRequest_ThumbnailDescriptor{
					ThumbnailDescriptor: thumbDesc,
				},
			}
		}

		start := time.Now()
		_, err = req.Client.UploadSidecars(context.Background(), &mimirpb.UploadSidecarsRequest{
			Asset: &mimirpb.AssetRef{
				Index:   req.Task.File().AssetRef().IndexName(),
				AssetId: req.Task.File().AssetRef().AssetID(),
			},
			Sidecars: result,
		})

		took := time.Since(start)

		if err != nil {
			req.Log.Error().Err(err).Msg("Unable to upload generated thumbnails")
			return err
		}

		req.Log.Debug().Str("took", fmt.Sprintf("%v", took)).Msg("Thumbnails uploaded")
	}

	return nil
}
